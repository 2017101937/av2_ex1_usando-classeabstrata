package Teste;

public class Produto {
	String nome;
	int cod;
	double preco;
	public Produto(String nome, int cod, double preco) {
		this.nome = nome;
		this.cod = cod;
		this.preco = preco;
	};
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getCod() {
		return cod;
	}
	public void setCod(int cod) {
		this.cod = cod;
	}
	public double getPreco() {
		return preco;
	}
	public void setPreco(double preco) {
		this.preco = preco;
	}
	
}
