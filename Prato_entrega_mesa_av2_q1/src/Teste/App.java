// Suponha um sistema de controle de pedidos num restaurante.
// Neste sistema, um cliente pode ser identificado pelo número 
// de sua mesa ou pelo número do seu telefone, no caso do pedido 
// ser entregue em domicílio. Um pedido contém o código do prato,
// o preço deste e o cliente correspondente. Os pedidos a serem
// entregues em domicílio tem acréscimo de 10% (Para acrescer um 
// valor de x%, basta multiplicar este valor por (1 + x/100).
// Baseado no código do método main(), informado abaixo,
// apresente uma implementação para as classes utilizadas neste código.
package Teste;

import java.util.Scanner;

public class App {
	
	   
    //variável comum
    static int index = 0, id=0;
    
    //Lista de contas
    static Pedido[] lista = new Pedido[10];
    
    static Produto[] cads = new Produto[20];
    
    static Scanner tecla = new Scanner(System.in);

	public static void main(String[] args) {
		int op = 0;
		do {
			System.out.println("O que deseja fazer!");
			System.out.println("1 - Cadastrar produto");
			System.out.println("2 - Fazer pedido");
			System.out.println("3 - Sair");
			op = tecla.nextInt();
			
			switch(op) {
			case 1: CadastrarProduto();break;
			case 2: FazerPedido();break;
			case 3: break;
			}
		}while(op!=3);

	}

	private static void CadastrarProduto() {
		System.out.println("Digite o nome: ");
		String nome = tecla.next();
		System.out.println("Digite o codigo: ");
		int cod = tecla.nextInt();
		System.out.println("Digite o preço: ");
		double preco = tecla.nextDouble();
		cads[id++] = new Produto(nome, cod, preco);
		
		
	}

	private static void FazerPedido() {
		double vu = 0;
		int count=0;
		System.out.println("Itens disponiveis: ");
		for(int i = 0; i<cads.length-1;i++) {
			if(cads[i]!=null) {
			System.out.println(cads[i].getCod()+" - "+cads[i].getNome());
			}
		}
		System.out.println("Digite o item desejado: ");
		int cod =tecla.nextInt();
		System.out.println("Digite a quantidade desejada: ");
		int qtd = tecla.nextInt();
		for(int i = 0; i<cads.length-1;i++) {
			if(cads[i].getCod()==cod) {
				vu = cads[i].getPreco();
				break;
			}else {
				count = count+1;
			}
		}if(count==cads.length-1) {
			System.out.println("Item não cadastrado!");
		}
		double valor = vu*qtd;
		System.out.println("1 - Entrega -- 2 - Consumo no local");
		int sit = tecla.nextInt();
		int tel = 0;
		int mesa = 0;
		int ig;
		if(sit==1) {
			System.out.println("Digite o telefone: ");
			tel = tecla.nextInt();
			lista[index++]= new Entrega(tel, valor);
			ig=index-1;
			lista[ig].print();
			
			
									
		}else if(sit==2){
			System.out.println("Digite o numero da mesa: ");
			mesa = tecla.nextInt();
			lista[index++] = new Local(mesa, valor);
			ig=index-1;
			lista[ig].print();
			
		} 
		
            
		
		}
		
	}


